﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class EsLoli
    {
        public static void Resolve()
        {
            Console.WriteLine("Beli 2 Gratis 1, Beli 3 Gratis 2");

            Console.WriteLine("Masukkan Harga Es Loli : ");
            int hargaEsLoli = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukkan Jumlah Uang : ");
            int uangDimiliki = int.Parse(Console.ReadLine());
         
            int esLoliYangDidapatkan = uangDimiliki / hargaEsLoli;  //digunakan untuk looping jumlah loli yang didapatkan dari maksimal uang
            int esloliBonus = 0;

            while (esLoliYangDidapatkan > 0) //looping untuk gratis es loli agar gratis bertambah saat melebihi 5 es loli
            {
                if (esLoliYangDidapatkan - 3 >= 0)  
                {
                    esloliBonus = esloliBonus + 2; // misal esloliyangdidapatkan = 5 => (5 - 3 > 0 maka kondisi 1 yang berfungsi
                    esLoliYangDidapatkan -= 3;
                }
                else if (esLoliYangDidapatkan - 2 >= 0)
                {
                    esloliBonus = esloliBonus + 1;
                    esLoliYangDidapatkan -= 2;
                }
                else
                {
                    esLoliYangDidapatkan -= 1; // dikurang 1 karena untuk es loli 1 tidak dapat bonus
                }
            }

            int sisaUang = uangDimiliki % hargaEsLoli;
            int esLoliTanpaBonus = uangDimiliki / hargaEsLoli;      //menghitung kembali es loli karena sudah abis saat looping while
            int esLoliAkhir = esLoliTanpaBonus + esloliBonus;

            Console.WriteLine("Es loli yang didapat adalah " + esLoliAkhir + ", " + "Sisa uang adalah " + sisaUang); //cetak perhitungan akhir

            Console.WriteLine();
        }
    }    
}
