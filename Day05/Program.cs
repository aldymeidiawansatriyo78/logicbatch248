﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Please input your Tugas question number : ");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("Bilangan prima");
                        Console.WriteLine();
                        BilanganPrima.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("EsLoli");
                        Console.WriteLine();
                        EsLoli.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Modus");
                        Console.WriteLine();
                        Modus.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("Pustaka");
                        Console.WriteLine();
                        Pustaka.Resolve();
                        break;

                    case 6:
                        Console.WriteLine("Kacamata dan Baju");
                        Console.WriteLine();
                        kacamataDanBaju.Resolve();
                        break;

                    case 7:
                        Console.WriteLine("Lilin Fibonacci");
                        Console.WriteLine();
                        LilinFibonacci.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("Int Geser");
                        Console.WriteLine();
                        IntGeser.Resolve();
                        break;

                    case 9:
                        Console.WriteLine("Parkir");
                        Console.WriteLine();
                        Parkir.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("Naik Gunung");
                        Console.WriteLine();
                        NaikGunung.Resolve();
                        break;

                    case 11:
                        Console.WriteLine("Kaos Kaki");
                        Console.WriteLine();
                        KaosKaki.Resolve();
                        break;

                    case 12:
                        Console.WriteLine("Pembulatan");
                        Console.WriteLine();
                        Pembulatan.Resolve();
                        break;

                    case 13:
                        Console.WriteLine("Es Loli");
                        Console.WriteLine();
                        EsLoliAlone.Resolve();
                        break;



                }

                Console.WriteLine("Continue?");
                answer = Console.ReadLine();
            }
        }
    }
}
