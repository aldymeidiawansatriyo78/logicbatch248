﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class LilinFibonacci
    {
        public static void Resolve()
        {
           Console.WriteLine("Input set of number : ");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);

            int index = 0;
            bool isNegative = true;

            while (index < numbersArray.Length)
            {
                if (numbersArray[index] < 0)
                {
                    isNegative = true;
                    break;
                }
                index++;
            }
            
            if (isNegative == false)
            {
                int[] fibonaciArray = new int[numbersArray.Length];
            double[] waktuHabisLilin = new double[numbersArray.Length];

            //input angka fibonaci ke array fibonaciArray
            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (i <= 1)
                {
                    fibonaciArray[i] = 1;
                }
                else
                {
                    fibonaciArray[i] = fibonaciArray[i - 1] + fibonaciArray[i - 2];
                }
                Console.Write(fibonaciArray[i] + " ");
            }
            Console.WriteLine();

            //menghitung waktu lilin habis saat dibakar
            for (int j = 0; j < numbersArray.Length; j++)
            {
                waktuHabisLilin[j] =(double) numbersArray[j] / fibonaciArray[j];
                Console.Write(waktuHabisLilin[j] + " ");
            }
            Console.WriteLine();

            //mencetak lilin yang paling cepat habis
            for (int k = 0; k < numbersArray.Length; k++)
            {
                if (waktuHabisLilin[k] == waktuHabisLilin.Min() )
                {                  
                    Console.Write("Lilin yang cepat habis adalah lilin ke - "+ (k + 1));
                    Console.WriteLine();
                }
            }
            }
        }
    }
}
