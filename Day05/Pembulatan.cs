﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class Pembulatan
    {
        public static void Resolve()
        {
           Console.WriteLine("Enter the set of number ");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToArray(numbers);
            int index = 0;
            bool isFalse = false;

            while (index < numbersArray.Length)
            {
                if (numbersArray[index] > 100)
                {
                    isFalse = true;
                }
                index++;
            }
            if (isFalse)
            {
                Console.WriteLine("Inputan salah");
            }
            else
            {
                for (int i = 0; i < numbersArray.Length; i++)
                {

                    if (numbersArray[i] < 45)
                    {
                        Console.Write(numbersArray[i] + " ");
                    }
                    else
                    {
                        int satuan = numbersArray[i] % 10;
                        if (satuan == 1)
                        {
                            numbersArray[i] = numbersArray[i] - 1;
                        }
                        else if (satuan == 2)
                        {
                            numbersArray[i] = numbersArray[i] - 2;
                        }
                        else if (satuan == 3)
                        {
                            numbersArray[i] = numbersArray[i] + 2;
                        }
                        else if (satuan == 4)
                        {
                            numbersArray[i] = numbersArray[i] + 1;
                        }
                        else if (satuan == 5)
                        {
                            numbersArray[i] = numbersArray[i];
                        }
                        else if (satuan == 6)
                        {
                            numbersArray[i] = numbersArray[i] - 1;
                        }
                        else if (satuan == 7)
                        {
                            numbersArray[i] = numbersArray[i] - 2;
                        }
                        else if (satuan == 8)
                        {
                            numbersArray[i] = numbersArray[i] + 2;
                        }
                        else if (satuan == 9)
                        {
                            numbersArray[i] = numbersArray[i] + 1;
                        }
                        Console.Write(numbersArray[i] + " ");
                    }
                }
            }
        }
    }
}
