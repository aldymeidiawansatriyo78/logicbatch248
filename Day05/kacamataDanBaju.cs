﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class kacamataDanBaju
    {
        public static void Resolve()
        {
            Console.WriteLine("Jumlah uang ");
            int uang = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan harga kacamata ");
            string numbers1 = Console.ReadLine();

            Console.WriteLine("Masukan harga baju ");
            string numbers2 = Console.ReadLine();

            int[] numbersArray1 = Utility.ConvertStringToIntArray1(numbers1);

            int[] numbersArray2 = Utility.ConvertStringToIntArray2(numbers2);

            int temp = 0;
            int maximum = 0;

            for (int i = 0; i < numbersArray1.Length; i++)
            {
                for (int j = 0; j < numbersArray2.Length; j++)
                {
                    if (numbersArray1[i] + numbersArray2[j] < uang)
                    {
                        temp = numbersArray1[i] + numbersArray2[j];

                        if (temp > maximum)
                        {
                            maximum = temp;
                        }
                    }
                }
            }

            Console.WriteLine(maximum);
        }
    }
}
