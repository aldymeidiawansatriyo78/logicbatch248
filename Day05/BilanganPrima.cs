﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class BilanganPrima
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan Panjang Bilangan prima : ");
            int lengthBilanganprima = int.Parse(Console.ReadLine());

            int prima = 2; //untuk yang ingin dicek bilangan primanya

            int lengthYangprima = 0; //menghitung banyak bilangan primanya

            while (lengthYangprima < lengthBilanganprima)
            {
                int faktorPrima = 0;

                for (int j = 1; j <= prima; j++) //loop untuk mengecek apakah bilangan prima ataubukan
                {
                    if (prima % j == 0)
                    {
                        faktorPrima += 1; //faktor bertambah apabila modulus bilangan prima
                    }
                }

                if (faktorPrima == 2) //bilangan prima apabila hanya ada dua faktor
                {
                    Console.Write(prima + " ");
                    lengthYangprima++;    //apabila prima terdeteksi maka length prima bertambah
                }

                prima++; // setiap selesai loop while prima bertambah 1
            }

            Console.WriteLine();
        }
    }
}
