﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class EsLoliAlone
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan Jumlah Uang : ");
            int uangDimiliki = int.Parse(Console.ReadLine());

            int harga = 3000;
            int esloliYangdidapatkan = uangDimiliki / harga;
            int esloliBonus = 0;

            while (0 < esloliYangdidapatkan)
            {
                if (esloliYangdidapatkan - 3 >= 0)
                {
                    esloliBonus = esloliBonus + 2;
                    esloliYangdidapatkan -= 3;
                }
                else if (esloliYangdidapatkan - 2 >= 0)
                {
                    esloliBonus = esloliBonus + 1;
                    esloliYangdidapatkan -= 2;
                }
                else
                {
                    esloliYangdidapatkan -= 1;
                }
            }

            int sisa = uangDimiliki % harga;

            int esloliTanpabonus = uangDimiliki / harga;

            int esloliAkhir = esloliTanpabonus + esloliBonus;

            Console.WriteLine("Es loli yang didapat adalah " + esloliAkhir + ", " + "Sisa uang adalah " + sisa);

            Console.WriteLine();
        }
    }
}
