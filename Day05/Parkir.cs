﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class Parkir
    {
        public static void Resolve()
        {
            using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class Parkir
    {
        public static void Resolve()
        {
            //validasi kepenulisan waktu
            try
            {
                Console.WriteLine("Masukkan Waktu Masuk : ");
                string stringMasuk = Console.ReadLine();

                Console.WriteLine("Masukkan Waktu Keluar : ");
                string stringKeluar = Console.ReadLine();

                //konversi datetime
                DateTime waktuMasuk = Convert.ToDateTime(stringMasuk);
                DateTime waktuKeluar = Convert.ToDateTime(stringKeluar);

                double selisihHari = (waktuKeluar - waktuMasuk).TotalDays;

                double lamaParkir = selisihHari * (double) 24;

                double biaya = 0;

                //validasi minus
                if (lamaParkir <= 0)
                {
                    Console.WriteLine("input yang anda masukkan salah!");
                }

                //kurang dari dua jam
                else if (lamaParkir < 2 && lamaParkir > 0)
                {
                    Console.WriteLine("Gratis");
                }

                //lebih dari dua jam dan kurang dari 24 jam
                else if (lamaParkir >= 2 && lamaParkir < 24)
                {
                    biaya = (lamaParkir - 1) * 4000;
                    biaya = biaya - (biaya % 4000);
                    Console.WriteLine(biaya);
                }

                //lebih dari 24 jam
                else
                {
                    Console.WriteLine(200000);
                }
                Console.WriteLine();
            }

            catch (Exception)
            {
                //memunculkan error sistem pada console
                Console.WriteLine("Format waktu salah!");
            }
        }
    }
}
        }
    }
}
