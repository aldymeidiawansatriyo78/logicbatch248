﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class IntGeser
    {
        public static void Resolve()
        {
           Console.WriteLine();
            //input baris angka
            Console.WriteLine("Masukkan baris angka: ");
            string angka = Console.ReadLine();
            int[] angkaArray = Utility.ConvertStringToIntArray(angka);

            //input mau berapa kali geser
            Console.WriteLine("Masukkan jumlah pergeseran: ");
            int pergeseran = int.Parse(Console.ReadLine());

            //pada perulangan akan dilakukan berapa kali pergeseran
            for (int i = 0; i < pergeseran; i++)
            {
                //geser kiri ke kanan
                //angka terakhir di array dimasukkan ke variabel terlebih dahulu
                int angkaArrayAkhir = angkaArray[angkaArray.Length - 1];

                //perulangan untuk menggeser array angkanya
                //karena pergeseran dilakukan dari kiri kekanan maka dimulai dari 
                //bilangan array dari yang dikiri dipindah kekanan atau sebelumnya

                for (int j = angkaArray.Length - 1; j > 0; j--)
                {
                    angkaArray[j] = angkaArray[j - 1];
                }

                //karena perulangan diatas untuk j > 0 maka nilai j = 0 adalah nilai angka terakhir baris array
                angkaArray[0] = angkaArrayAkhir;
            }

            //cetak baris angka setelah dilakukan pergeseran
            for (int i = 0; i < angkaArray.Length; i++)
            {
                Console.Write(angkaArray[i] + " ");
            }
            Console.WriteLine();
        }
    }
}
