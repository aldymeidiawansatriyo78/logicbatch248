﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class MarsExploration
    {
        public static void Resolve()
        {
            Console.WriteLine("Please input SOS signal");
            string signal = Console.ReadLine().ToUpper();

            int countS = 0;
            int countO = 0;


            if (signal.Length % 3 != 0)
            {
                Console.WriteLine("Inputan Salah");
            }
            else
            {
                for (int i = 0; i < signal.Length; i++)
                {
                    if (i % 3 == 0 || i % 3 == 2)
                    {
                        if (signal[i] != 'S')
                        {
                            countS++;
                        }
                    }
                    if (i % 3 == 1)
                    {
                        if (signal[i] != 'O')
                        {
                            countO++;
                        }
                    }               
                }
                int total = countS + countO;

                Console.WriteLine(total);

                Console.WriteLine();
            }
        }
    }
}
