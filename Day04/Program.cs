﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Please input your String question number : ");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("Camel Case");
                        Console.WriteLine();
                        CamelCase.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Strong Password");
                        Console.WriteLine();
                        StrongPassword.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Caesar Cipher");
                        Console.WriteLine();
                        CaesarCipher.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("Mars Exploration");
                        Console.WriteLine();
                        MarsExploration.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("HackerRank in A String");
                        Console.WriteLine();
                        HackerRankInAString.Resolve();
                        break;

                    case 6:
                        Console.WriteLine("Pangrams");
                        Console.WriteLine();
                        Pangrams.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("GemStones");
                        Console.WriteLine();
                        GemStones.Resolve();
                        break;

                    case 9:
                        Console.WriteLine("Making Anagrams");
                        Console.WriteLine();
                        MakingAnagrams.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("Two Strings");
                        Console.WriteLine();
                        TwoStrings.Resolve();
                        break;

                    case 11:
                        Console.WriteLine("Middle Asterisk 3");
                        Console.WriteLine();
                        MiddleAsterisk3.Resolve();
                        break;

                    case 12:
                        Console.WriteLine("Palindrome");
                        Console.WriteLine();
                        Palindrome.Resolve();
                        break;
                }

                Console.WriteLine("Continue?");
                answer = Console.ReadLine();
            }
        }
    }
}
