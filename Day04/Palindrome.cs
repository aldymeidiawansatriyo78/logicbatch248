﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Palindrome
    {
        public static void Resolve()
        {
            Console.WriteLine("PleaseEnter the text : ");
            string text = Console.ReadLine();
            string conclusion = "";

            int length = text.Length;

            for (int i = length - 1; i >= 0; i--)
            {
                conclusion = conclusion + text[i];
            }

            int help1 = 0;
            int help2 = 0;

            for (int i = 0; i < length; i++)
            {
                if (length == 0)
                {
                    Console.WriteLine("Incorrect words entered");
                }
                else
                {
                    if (conclusion == text)
                    {
                        help1 += 1;
                    }
                    else
                    {
                        help2 += 0;
                    }
                }
            }
            if (help1 > 0)
            {
                Console.WriteLine("YES");
            }
            else if (help2 == 0)
            {
                Console.WriteLine("NO");
            }
        }
    }
}
