﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class CaesarCipher
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan Input Kalimat : ");
            string input = Console.ReadLine();
            Console.WriteLine("Input number of rotate : ");
            int shift = Convert.ToInt32(Console.ReadLine());

            int[] alphabet = new int[input.Length];
            char[] rotate = new char[input.Length];
            for (int i = 0; i < input.Length; i++)
            {
                alphabet[i] = (int)input[i];
                if (alphabet[i] > 64 && alphabet[i] < 91)
                {
                    //% modulus 26 untuk shift di atas 26
                    
                    alphabet[i] = (97 + ((alphabet[i] - 97) + shift) % 26);
                }
                else if (alphabet[i] > 96 && alphabet[i] < 123)
                {
                    //% modulus 26 untuk shift di atas 26
                    alphabet[i] = (97 + ((alphabet[i] - 97) + shift) % 26);
                }
                rotate[i] = (char)alphabet[i];
                Console.WriteLine(rotate[i]);
            }
        }
    }
}
