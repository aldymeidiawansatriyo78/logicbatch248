﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class StrongPassword
    {
        public static void Resolve()
        {
            Console.WriteLine("Please input Setences ");
            string setences = Console.ReadLine();

            char[] number = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '0' };
            char[] lowerCase = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
            char[] upperCase = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            char[] specialCharacters = new char[] { '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '+' };

            int count = 5;
            int countAll = 0;

            foreach (char c in setences)
            {
                countAll += 1;
            }

            int totalChar = countAll;

            if (totalChar >= 6)
            {
                count -= 1;
            }

            if (setences.Contains(number.ToString()))
            {
                count -= 1;
            }

            if (setences.Contains(upperCase.ToString()))
            {
                count -= 1;
            }

            if (setences.Contains(lowerCase.ToString()))
            {
                count -= 1;
            }

            if (setences.Contains(specialCharacters.ToString()))
            {
                count -= 1;
            }

            int total = count;

            Console.WriteLine("You must add " + total + " character more");

            Console.WriteLine();
        }
    }
}
