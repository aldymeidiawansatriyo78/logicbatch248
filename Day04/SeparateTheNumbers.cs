﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class SeparateTheNumbers
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan angka-angka yang ingin diurutkan");
            string numberString = Console.ReadLine();

            bool condition = false;

            for (int i = 0; i < numberString.Length/2; i++)
            {
                int number = int.Parse(numberString.Substring(0, i + 1));
                string result="";
                int index = 0;
                while (result.Length<numberString.Length)
                {
                    result += (number + index).ToString();
                    index++;
                }

                if (result.Equals(numberString))
                {
                    condition = true;
                    break;
                }
            }

            if (condition)
            {
                Console.WriteLine("YES");
            }
            else
            {
                Console.WriteLine("NO");
            }

        }
    }
}
