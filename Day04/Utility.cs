﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Utility
    {
        public static int[] ConvertStringToIntArray(string setences)
        {
            string[] stringSetencesArray = setences.Split(' ');
            int[] setencesArray = new int[stringSetencesArray.Length];

            //Convert to int
            for (int i = 0; i < setencesArray.Length; i++)
            {
                setencesArray[i] = int.Parse(stringSetencesArray[i]);
            }

            return setencesArray;
        }
    }
}
