﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Pangrams
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan kalimat");
            string kalimatAwal = Console.ReadLine();
            //kalimatAwal = "The quick brown fox jumps over The lazy dogg";
            string kalimatLower = kalimatAwal.ToLower();
            char[] kalimatLowerChar = kalimatLower.ToCharArray();
            string alphabet = "abcdefghijklmnopqrstuvwxyz";
            char[] alphabetChar = alphabet.ToCharArray();

            int jumlah = 0;

            for (int i = 0; i < alphabetChar.Length; i++)
            {
                for (int j = 0; j < kalimatLower.Length; j++)
                {
                    if (jumlah < alphabetChar.Length)
                    {
                        if (alphabetChar[i] == kalimatLowerChar[j])
                        {
                            jumlah++;
                        }
                    }                     
                }
            }

            if (jumlah == 26)
            {
                Console.WriteLine("Pangrams");
            }

            else
            {
                Console.WriteLine("Not Pangrams");
            }
        }
    }
}
