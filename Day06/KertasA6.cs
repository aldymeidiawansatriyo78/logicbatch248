﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class KertasA6
    {
        public static void Resolve()
        {
            Console.Write("berapakah kertas A6 untuk membuat A");
            int kertasA6 = int.Parse(Console.ReadLine());

            Console.WriteLine();

            int jumlahA6 = 1;
    
            if (kertasA6 > 6 && kertasA6 < 0)
            {
                Console.WriteLine("maaf, input salah");
            }
            else if (kertasA6 < 6 && kertasA6 >= 0)
            {
                if (kertasA6 == 5 )
                {
                    jumlahA6 *= 2;
                }
                else if (kertasA6 == 4)
                {
                    jumlahA6 *= 4;
                }
                else if (kertasA6 == 3)
                {
                    jumlahA6 *= 8;
                }
                else if (kertasA6 == 2)
                {
                    jumlahA6 *= 16;
                }
                else if (kertasA6 == 1)
                {
                    jumlahA6 *= 32;
                }
                else if (kertasA6 == 0)
                {
                    jumlahA6 *= 64;
                }

                Console.WriteLine("jumlah kertas A6 yang dibutuhkan sebanyak " + jumlahA6);
            }

            Console.WriteLine();
        }
    }
}
