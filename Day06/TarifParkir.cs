﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class TarifParkir
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan tanggal dan jam Masuk Parkir : ");
            string jamMasukString = Console.ReadLine();

            Console.WriteLine("Masukkan tanggal dan jam Keluar Parkir : ");
            string jamKeluarString = Console.ReadLine();

            DateTime waktuMasuk = Convert.ToDateTime(jamMasukString);
            DateTime waktuKeluar = Convert.ToDateTime(jamKeluarString);

            int selisihHariLength = (waktuKeluar - waktuMasuk).Days;
            double selisihHariParkir = (waktuKeluar - waktuMasuk).TotalDays;

            double totalLamaParkir = selisihHariParkir * (double)24; 
            double biayaParkir = 0;
            double biayaParkir1 = 0;

            for (int i = 0; i <= selisihHariLength; i++)
            {
                if (totalLamaParkir < 24)
                {
                    if (totalLamaParkir <= 8)
                    {
                        biayaParkir = 1000 * totalLamaParkir;
                    }

                    else if (totalLamaParkir >= 8 && totalLamaParkir < 24)
                    {
                        biayaParkir += 8000;
                    }
                }
                else
                {

                }
            }
            Console.WriteLine("Jumlah biaya parkir yang harus dibayarkan adalah " + biayaParkir);

            Console.WriteLine();
        }
    }
}
