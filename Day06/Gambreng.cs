﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class Gambreng
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan Input 1");
            string input1 = Console.ReadLine();

            Console.WriteLine("Masukkan input 2");
            string input2 = Console.ReadLine();

            char[] charInput1Array = input1.ToCharArray();
            char[] charInput2Array = input2.ToCharArray();

            int countMenangA = 0;
            int countKalahA = 0;
            int countMenangB = 0;
            int countKalahB = 0;
            int countSeri = 0;

            if(input1.Length != input2.Length)
            {
                Console.WriteLine("maaf inputan anda salah coba sekali lagi");
            }
            else
            {
                for (int i = 0; i < input1.Length; i++)
                {
                    if (charInput1Array[i] == 'G' && charInput2Array[i] == 'K' || charInput1Array[i] == 'B' && charInput2Array[i] == 'G' || charInput1Array[i] == 'K' && charInput2Array[i] == 'B')
                    {
                        countMenangA += 1;
                        countKalahB += 1;
                    }
                    else if (charInput1Array[i] == 'K' && charInput2Array[i] == 'G' || charInput1Array[i] == 'G' && charInput2Array[i] == 'B' || charInput1Array[i] == 'B' && charInput2Array[i] == 'K')
                    {
                        countKalahA += 1;
                        countMenangB += 1;
                    }
                    else if (charInput1Array[i] == charInput2Array[i])
                    {
                        countSeri += 1;
                    }
                }

                int totalB = countMenangB + countSeri;
                int totalA = countMenangA + countSeri;

                Console.WriteLine("B menang " + countMenangB + " Kalah " + countKalahB + " seri " + countSeri);
                Console.WriteLine("A menang " + countMenangA + " Kalah " + countKalahA + " seri " + countSeri);

                if (totalB > totalA)
                {
                    Console.WriteLine("B Menang");
                }
                else if (totalB < totalA)
                {
                    Console.WriteLine("A Menang");
                }
                else
                {
                    Console.WriteLine("SERI");
                }
            }
        }
    }
}
