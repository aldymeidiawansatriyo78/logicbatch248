﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Please input your Latihan question number : ");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {

                    case 1:
                        Console.WriteLine("Kertas A6");
                        Console.WriteLine();
                        KertasA6.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Tarif Parkir");
                        Console.WriteLine();
                        TarifParkir.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("Gambreng");
                        Console.WriteLine();
                        Gambreng.Resolve();
                        break;



                }

                Console.WriteLine("Continue?");
                answer = Console.ReadLine();
            }
        }
    }
}
