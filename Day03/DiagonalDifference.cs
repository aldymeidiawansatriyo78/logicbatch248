﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class DiagonalDifference
    {
        public static void Resolve()
        {
            Console.WriteLine("Input Length : ");
            int length = int.Parse(Console.ReadLine());

            int[,] array2D = new int[length, length];

            //Input Value
            for (int i = 0; i < length; i++)
            {
                Console.WriteLine("Enter the " + (i + 1) + " set of number : ");
                string numbers = Console.ReadLine();

                int[] array = Utility.ConvertStringToIntArray(numbers);
                if (array.Length != length)
                {
                    Console.WriteLine("Wrong Numbers, try again");
                    i = -1;
                }
                else
                {
                    for (int j = 0; j < length; j++)
                    {
                        array2D[i, j] = array[j];
                    }
                }
            }

            //Memasukkan Value

            int angka = 0;
            int angka2 = 0;

            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    if (i == j)
                    {
                        angka = angka + array2D[i, j];
                    }
                }
            }

            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    if ((i + j) == (array2D.GetLength(1) - 1))
                    {
                        angka2 = angka2 + array2D[i, j];
                    }            
                }
            }
            Console.WriteLine();

            Console.WriteLine("The result of first Diagonal is " + angka);
            Console.WriteLine("The result of Second Diagonal is " + angka2);

            Console.WriteLine("The Result of The absolute of two diagonal is " + Math.Abs(angka - angka2));

            Console.WriteLine();
        }
    }
}
