﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class aVeryBigSum
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the set of number");
            string numbers = Console.ReadLine();

            //input Value
            long[] numbersArray2 = Utility.ConvertStringToLongArray
                (numbers);

            long total = 0;

            for (int i = 0; i < numbersArray2.Length; i++)
            {
                total += numbersArray2[i];
            }

            Console.WriteLine("The Result is " + total);
        }
    }
}
