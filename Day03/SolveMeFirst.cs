﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class SolveMeFirst
    {
        public static void Resolve()
        {
            Console.WriteLine("Please input variabel a :");
            int number1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Please input variabel b :");
            int number2 = int.Parse(Console.ReadLine());

            int total = Utility.Sum(number1, number2);

            Console.WriteLine("The result is " + total);

            Console.ReadKey();
        }
    }
}
