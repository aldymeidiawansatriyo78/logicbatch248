﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                string answer = "Y";

                while (answer.ToUpper() == "Y")
                {
                    Console.WriteLine("Please input your hackerank question number : ");
                    int nomorSoal = int.Parse(Console.ReadLine());

                    switch (nomorSoal)
                    {
                        case 1:
                            Console.WriteLine("Solve Me First");
                            SolveMeFirst.Resolve();
                            break;

                        case 2:
                            Console.WriteLine("Time Convertion");
                            TimeConvertion.Resolve();
                            break;

                        case 3:
                            Console.WriteLine("Simple ArraySum ");
                            SimpleArraySum.Resolve();
                            break;

                        case 4:
                            Console.WriteLine("Diagonal Difference");
                            DiagonalDifference.Resolve();
                            break;

                        case 5:
                            Console.WriteLine("Plus Minus");
                            PlusMinus.Resolve();
                            break;

                        case 6:
                            Console.WriteLine("Staircase");
                            Staircase.resolve();
                            break;

                        case 7:
                            Console.WriteLine("Mini-Max Sum");
                            Mini_MaxSum.Resolve();
                            break;

                        case 8:
                            Console.WriteLine("Birthday Cake Candles");
                            BirthdayCakeCandles.Resolve();
                            break;

                        case 9:
                            Console.WriteLine("A Very Big Sum");
                            aVeryBigSum.Resolve();
                            break;

                        case 10:
                            Console.WriteLine("Compare The Triplets");
                            CompareTheTriplets.Resolve();
                            break;

                        default:
                            Console.WriteLine("Cannot Found, Please Try Again");
                            break;
                    }

                    Console.WriteLine("Continue?");
                    answer = Console.ReadLine();
                }

            }
        }
    }
}