﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Staircase
    {
        public static void resolve()
        {
            Console.WriteLine("Masukkan nilai n");
            int n = int.Parse(Console.ReadLine());

            int[,] array2D = new int[n, n];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if ((i + j) >= (n - 1))
                    {
                        Console.Write("#");
                    }  
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
