﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class TimeConvertion
    {
        public static void Resolve()
        {
            Console.WriteLine("input the time");
            string time = Console.ReadLine();

            if (!time.Contains("AM") && !time.Contains("PM"))
            {
                Console.WriteLine("Format waktu yang anda masukkan salah!");
            }
            else
            {
                try
                {
                    DateTime dateTime = Convert.ToDateTime(time);
                    Console.Write("Format 24 Jam: ");
                    Console.WriteLine(dateTime.ToString("HH:mm:ss"));
                }
                catch (Exception)
                {
                    Console.WriteLine("angka yang anda masukkan salah");
                    throw;
                }
            }
        }
    }
}
