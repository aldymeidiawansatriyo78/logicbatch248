﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class BirthdayCakeCandles
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the set of number");
            string numbers = Console.ReadLine();

            //input Value
            int[] numbersArray = Utility.ConvertStringToIntArray
                (numbers);

            int maksimal = numbersArray.Max();
            int total = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (numbersArray[i] == maksimal)
                {
                    total++;
                }
                else
                {
                    //Do Nothing
                }
            }

            Console.WriteLine("Count of Tallest is " + total);

            Console.WriteLine();


        }
    }
}
