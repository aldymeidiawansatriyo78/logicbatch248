﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Utility
    {
        public static int Sum(int number1, int number2)
        {
            int total = number1 + number2;

            return total;
        }

        public static int[] ConvertStringToIntArray(string numbers)
        {
            string[] stringNumbersArray = numbers.Split(' ');
            int[] numbersArray = new int[stringNumbersArray.Length];

            //Convert to int
            for (int i = 0; i < numbersArray.Length; i++)
            {
                numbersArray[i] = int.Parse(stringNumbersArray[i]);
            }

            return numbersArray;
        }
        public static long[] ConvertStringToLongArray(string numbers)
        {
            string[] stringNumbersArray2 = numbers.Split(' ');
            long[] numbersArray2 = new long[stringNumbersArray2.Length];

            //Convert to int
            for (int i = 0; i < numbersArray2.Length; i++)
            {
                numbersArray2[i] = long.Parse(stringNumbersArray2[i]);
            }

            return numbersArray2;
        }
    }
}
