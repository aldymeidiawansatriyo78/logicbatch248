﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class CompareTheTriplets
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the set of number");
            string numbers = Console.ReadLine();

            Console.WriteLine("Enter the set of number");
            string numbers2 = Console.ReadLine();

            //Input Value
            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);
            int[] numbers2Array = Utility.ConvertStringToIntArray(numbers2);


            if (numbersArray.Length != numbers2Array.Length)
            {
                Console.WriteLine("Wrong Numbers, try again");
            }
            else
            {
                int alice = 0;
                int bob = 0;

                for (int i = 0; i < numbersArray.Length; i++)
                {
                    if (numbersArray[i] > numbers2Array[i])
                    {
                        alice += 1;
                    }
                    else if (numbersArray[i] < numbers2Array[i])
                    {
                        bob += 1;
                    }
                }

                //Memasukkan Value

                int skorAlice = alice;
                int skorBob = bob;

                Console.WriteLine("Alice Score is : " + skorAlice);
                Console.WriteLine("Bob Score is : " + skorBob);

                Console.WriteLine();
            }          
        }
    }
}
