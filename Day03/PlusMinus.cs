﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class PlusMinus
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the set of number");
            string numbers = Console.ReadLine();

            //input Value
            int[] numbersArray = Utility.ConvertStringToIntArray
                (numbers);

            double total = 0;
            double negatif = 0;
            double positif = 0;
            double zero = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                total = numbersArray.Length;
                if (numbersArray[i] < 0)
                {
                    negatif++;
                }
                else if (numbersArray[i] > 0)
                {
                    positif++;
                }
                else
                {
                    zero++;
                }
            }

            //Menghitung Value
            double totalPositif = positif / total;
            double totalNegatif = negatif / total;
            double totalZero = zero / total;

            Console.WriteLine("Count of Positive Number : " + totalPositif);

            Console.WriteLine("Count of Negative Number : " + totalNegatif);

            Console.WriteLine("Count of Zero Number : " + totalZero);

            Console.WriteLine();
        }
    }
}