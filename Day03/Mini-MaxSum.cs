﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Mini_MaxSum
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the set of number");
            string numbers = Console.ReadLine();

            //input Value
            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);

            int maksimal = numbersArray.Max();
            int minimal = numbersArray.Min();
            
            int total = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                total += numbersArray[i];
            }

            int totalMaksimal = total - maksimal;
            int totalMinimal = total - minimal;

            Console.WriteLine("Maximum Value is" + totalMaksimal);
            Console.WriteLine("Minimum Value is" + totalMinimal);

            Console.WriteLine();
        }
    }
}
