﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02
{
    class Soal05
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan nilai n");
            int n = int.Parse(Console.ReadLine());

            int[,] array2D = new int[3, n];

            //Value Creation
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        array2D[i , j] = j;
                    }
                    else 
                    {
                        if (j < 1)
                        {
                            array2D[i , j] = array2D[i - 1, j + (n - 1)] + 1;                         
                        }
                        else
                        {
                            array2D[i , j] = array2D[i, j - 1] + 1;
                        }
                    }
                }
            }

            Utility.PrintArray2D(array2D);
        }
    }
}
