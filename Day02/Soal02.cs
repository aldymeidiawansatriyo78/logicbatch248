﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02
{
    class Soal02
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan nilai n");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine("masukkan nilai n2");
            int n2 = int.Parse(Console.ReadLine());

            int[,] array2D = new int[2, n];

            //Value Creation
            for (int i = 0; i <2; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        array2D[i, j] = j;
                    }
                    else
                    {
                        if ((j + 1) % 3 == 0)
                        {
                            array2D[i, j] = Convert.ToInt32(Math.Pow(n2, j));
                            array2D[i, j] *= -1;
                        }
                        else
                        {
                            array2D[i, j] = Convert.ToInt32(Math.Pow(n2, j));
                        }
                    }
                }

            }
            Utility.PrintArray2D(array2D);
        }
    }
}
