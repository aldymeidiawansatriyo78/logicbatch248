﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Nomor05
    {
        public static void Resolve()
        {         
            int tLakiDewasa = 0;
            int tPerempuanDewasa = 0;
            int tRemaja = 0;
            int tAnak = 0;
            int tBalita = 0;

            string answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("***Berikut pilihan indeks orang yang ingin makan***");
                Console.WriteLine("1. Laki-laki Dewasa, 2. Perempuan Dewasa, 3. Anak-anak, 4. Remaja, 5. Balita, ");
                Console.WriteLine("Masukkan indeks orang yang ingin ditambah : ");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("Masukkan Jumlah orang Laki - laki Dewasa");
                        int lakiDewasa = int.Parse(Console.ReadLine());
                        tLakiDewasa += lakiDewasa;
                        break;

                    case 2:
                        Console.WriteLine("Masukkan Jumlah orang Perempuan Dewasa");
                        int perempuanDewasa = int.Parse(Console.ReadLine());
                        tPerempuanDewasa += perempuanDewasa;
                        break;

                    case 3:
                        Console.WriteLine("Masukkan Jumlah orang Remaja");
                        int remaja = int.Parse(Console.ReadLine());
                        tRemaja += remaja;
                        break;

                    case 4:
                        Console.WriteLine("Masukkan Jumlah orang Anak-anak");
                        int anak = int.Parse(Console.ReadLine());
                        tAnak += anak;
                        break;
                        
                    case 5:
                        Console.WriteLine("Masukkan Jumlah orang Balita");
                        int balita = int.Parse(Console.ReadLine());
                        tBalita += balita;
                        break;
                }

                Console.WriteLine("Apakah ingin menambah orang?");
                answer = Console.ReadLine();
            }

            int totalOrang = tLakiDewasa + tPerempuanDewasa + tRemaja + tAnak + tBalita;

            int jumlahPorsi = 0;

            if (totalOrang <= 5)
            {
                jumlahPorsi += (tLakiDewasa * 2) + (tPerempuanDewasa * 1) + (tRemaja * 1) + (tAnak / 2) + (tBalita * 1); 
            }
            else if (totalOrang > 5 && totalOrang % 2 == 0)
            {
                jumlahPorsi += (tLakiDewasa * 2) + (tPerempuanDewasa * 1) + (tRemaja * 1) + (tAnak / 2) + (tBalita * 1);
            }
            else if (totalOrang > 5 && tPerempuanDewasa == 0)
            {
                jumlahPorsi += (tLakiDewasa * 2) + (tPerempuanDewasa * 1) + (tRemaja * 1) + (tAnak / 2) + (tBalita * 1);
            }
            else if (totalOrang > 5 && totalOrang % 2 != 0)
            {
                jumlahPorsi += (tLakiDewasa * 2) + ((tPerempuanDewasa + 1) * 1) + (tRemaja * 1) + (tAnak / 2) + (tBalita * 1);
            }

            Console.WriteLine();

            Console.WriteLine("Jumlah Porsi yang dimakan adalah " + jumlahPorsi);

            Console.WriteLine();
        }                 
    }
}
