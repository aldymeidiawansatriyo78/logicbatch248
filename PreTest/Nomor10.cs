﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Nomor10
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan panjang Array bilangan : ");
            int length = int.Parse(Console.ReadLine());

            int[] fibonacciArray = Utility.fibonacciArray(length);
            int[] primaArray = Utility.primaArray(length);

            Utility.PrintArray(fibonacciArray);
            Utility.PrintArray(primaArray);

            int penjumlahanArray = 0;

            for (int i = 0; i < length; i++)
            {
                penjumlahanArray = fibonacciArray[i] + primaArray[i];
                Console.Write(penjumlahanArray + "  ");
            }

            Console.WriteLine();
        }
    }
}
