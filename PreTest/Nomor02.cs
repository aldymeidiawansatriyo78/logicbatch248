﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Nomor02
    {
        public static void Resolve()
        {
            Console.Write("Masukkan Rute yang ingin dilalui : ");
            string rute = Console.ReadLine();

            string[] ruteArray = rute.Split('-');

            double totalPerjalanan = 0;

            for (int i = 0; i < ruteArray.Length; i++)
            {
                if (i == 0)
                {
                    totalPerjalanan += 0;
                }
                else if(ruteArray[i - 1] == "toko")
                {
                    if (ruteArray[i] == "tempat1")
                    {
                        totalPerjalanan += 2000;
                    }
                    else if (ruteArray [i] == "tempat2")
                    {
                        totalPerjalanan += 2500;
                    }
                    else if (ruteArray[i] == "tempat3")
                    {
                        totalPerjalanan +=  4000;
                    }
                    else if (ruteArray[i] == "tempat4")
                    {
                        totalPerjalanan += 6500;
                    }
                }
                else if (ruteArray[i - 1] == "tempat1")
                {
                    if (ruteArray[i] == "tempat2")
                    {
                        totalPerjalanan += 500;
                    }
                    else if (ruteArray[i] == "tempat3")
                    {
                        totalPerjalanan += 2000;
                    }
                    else if (ruteArray[i] == "tempat4")
                    {
                        totalPerjalanan += 4500;
                    }
                    else if (ruteArray[i] == "toko")
                    {
                        totalPerjalanan += 2000;
                    }
                }
                else if (ruteArray[i - 1] == "tempat2")
                {
                    if (ruteArray[i] == "tempat1")
                    {
                        totalPerjalanan +=  500;
                    }
                    else if (ruteArray[i] == "tempat3")
                    {
                        totalPerjalanan +=  1500;
                    }
                    else if (ruteArray[i] == "tempat4")
                    {
                        totalPerjalanan += 4000;
                    }
                    else if (ruteArray[i] == "toko")
                    {
                        totalPerjalanan += 2500;
                    }
                }
                else if (ruteArray[i - 1] == "tempat3")
                {
                    if (ruteArray[i] == "tempat1")
                    {
                        totalPerjalanan += 2000;
                    }
                    else if (ruteArray[i] == "tempat2")
                    {
                        totalPerjalanan += 1500;
                    }
                    else if (ruteArray[i] == "tempat4")
                    {
                        totalPerjalanan += 2500;
                    }
                    else if (ruteArray[i] == "toko")
                    {
                        totalPerjalanan += 4000;
                    }
                }
                else if (ruteArray[i - 1] == "tempat4")
                {
                    if (ruteArray[i] == "tempat3")
                    {
                        totalPerjalanan += +2500;
                    }
                    else if (ruteArray[i] == "tempat2")
                    {
                        totalPerjalanan += 4000;
                    }
                    else if (ruteArray[i] == "tempat1")
                    {
                        totalPerjalanan += 4500;
                    }
                    else if (ruteArray[i] == "toko")
                    {
                        totalPerjalanan += 6000;
                    }
                }
            }

            double totalBensin = totalPerjalanan / 2500;

            Console.WriteLine("Total bensin yang dikeluarkan adalah " + totalBensin);
        }
    }
}
