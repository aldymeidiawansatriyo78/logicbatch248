﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Nomor06
    {
        public static void Resolve()
        {
            Console.Write("Tentukan siapakah yang main lebih dahulu : ");
            int start = int.Parse(Console.ReadLine());

            if (start == 1)
            {
                Console.Write("Masukkan angka player 1 : ");
                int player11 = int.Parse(Console.ReadLine());

                Random r = new Random();
                int komputer11 = r.Next(0, 9);
                Console.Write("Angka Player Komputer adalah : " + komputer11);

                Console.WriteLine();

                if(komputer11 > player11)
                {
                    Console.WriteLine("YOU LOOSE");
                }
                else
                {
                    Console.WriteLine("YOU WIN");
                }                
            }
            else if (start == 2)
            {
                Random r = new Random();
                int komputer11 = r.Next(0, 9);

                Console.Write("Masukkan angka player 1 : ");
                int player11 = int.Parse(Console.ReadLine());

                if (komputer11 > player11)
                {
                    Console.WriteLine("YOU LOOSE");
                }
                else
                {
                    Console.WriteLine("YOU WIN");
                }
            }

            Console.WriteLine();
        }
    }
}
