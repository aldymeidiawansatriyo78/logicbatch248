﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Nomor08
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan PIN : ");
            int pin = int.Parse(Console.ReadLine());

            if (pin != 123456)
            {
                Console.WriteLine("PIN SALAH");
            }
            else
            {
                int saldo = 0;

                string answer = "Y";

                while (answer.ToUpper() == "Y")
                {
                    Console.WriteLine("pilih 1. untuk cek saldo, pilih 2. untuk setor uang, pilih 3. untuk transfer uang");
                    int pilih = int.Parse(Console.ReadLine());

                    switch (pilih)
                    {
                        case 1:
                            Console.WriteLine("Saldo anda adalah sebesar " + saldo);
                            break;

                        case 2:
                            Console.WriteLine("Masukkan jumlah uang yang akan ditransfer : ");
                            int uangTransfer = int.Parse(Console.ReadLine());

                            saldo += uangTransfer;

                            Console.WriteLine("Transaksi Berhasil, saldo anda menjadi " + saldo);

                            break;

                        case 3:

                            int bantu = 0;

                            while (bantu == 0)
                            {
                                Console.WriteLine("Masukkan no Rekening tujuan : ");
                                string rekening = Console.ReadLine();

                                char[] rekeningChar = rekening.ToCharArray();

                                if (rekeningChar.Length > 10 && rekeningChar.Length < 10)
                                {
                                    Console.WriteLine("No rekening salah, silahkan input lagi");
                                    bantu += 0;
                                }
                                else if (rekeningChar.Length == 10)
                                {
                                    Console.WriteLine("Masukkan nominal transfer : ");
                                    int nomTransfer = int.Parse(Console.ReadLine());

                                    saldo -= nomTransfer;                                 
                                    
                                    if (saldo < nomTransfer)
                                    {
                                        Console.WriteLine("Maaf Saldo anda tidak cukup");
                                        bantu += 0;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Transaksi Berhasil, Saldo anda sisa " + saldo);
                                        bantu += 1;
                                    }                               
                                }
                            }

                            break;         
                    }

                    Console.WriteLine("Apakah anda ingin bertransaksi lagi?");
                    answer = Console.ReadLine();
                }
            }
        }
    }
}
