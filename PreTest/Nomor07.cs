﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Nomor07
    {
        public static void Resolve()
        {
            Console.WriteLine("Keranjang manakah yang akan kosong : ");
            string keranjangKosong = Console.ReadLine();

            Console.WriteLine("Keranjang manakah yang akan dibawa : ");
            string keranjangBawa = Console.ReadLine();

            int totalIsiKeranjang = 0;

            if (keranjangKosong == "Keranjang 1")
            {
                Console.Write("Masukkan isi Keranjang 2 : ");
                int keranjang1 = int.Parse(Console.ReadLine());

                Console.WriteLine();

                Console.Write("Masukkan isi keranjang 3 : ");
                int keranjang2 = int.Parse(Console.ReadLine());

                Console.WriteLine();

                if (keranjangBawa == "Keranjang 1")
                {
                    totalIsiKeranjang += keranjang1 + keranjang2;
                }
                else if (keranjangBawa == "Keranjang 2") 
                {
                    totalIsiKeranjang += 0 + keranjang2;
                }
                else if (keranjangBawa == "Keranjang 3") 
                {
                    totalIsiKeranjang += 0 + keranjang1;
                }

            }
            else if (keranjangKosong == "Keranjang 2")
            {
                Console.Write("Masukkan isi Keranjang 1 : ");
                int keranjang1 = int.Parse(Console.ReadLine());

                Console.WriteLine();

                Console.Write("Masukkan isi keranjang 3 : ");
                int keranjang2 = int.Parse(Console.ReadLine());

                Console.WriteLine();

                if (keranjangBawa == "Keranjang 1")
                {
                    totalIsiKeranjang += 0 + keranjang2;
                }
                else if (keranjangBawa == "Keranjang 2") 
                {
                    totalIsiKeranjang += keranjang1 + keranjang2;
                }
                else if (keranjangBawa == "Keranjang 3")
                {
                    totalIsiKeranjang += 0 + keranjang1;
                }
            }
            else if (keranjangKosong == "Keranjang 3")
            {
                Console.Write("Masukkan isi Keranjang 1 : ");
                int keranjang1 = int.Parse(Console.ReadLine());

                Console.WriteLine();

                Console.Write("Masukkan isi keranjang 2 : ");
                int keranjang2 = int.Parse(Console.ReadLine());

                Console.WriteLine();

                if (keranjangBawa == "Keranjang 1")
                {
                    totalIsiKeranjang += 0 + keranjang2;
                }
                else if (keranjangBawa == "Keranjang 2")
                {
                    totalIsiKeranjang += keranjang1 + 0;
                }
                else if (keranjangBawa == "Keranjang 3")
                {
                    totalIsiKeranjang += keranjang2 + keranjang1;
                }
            }

            Console.WriteLine("Total isi Keranjang saat ini adalah " + totalIsiKeranjang);

            Console.WriteLine();
        }
    }
}
