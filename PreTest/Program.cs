﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukkan Nomor soal PreTest : ");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("Nomor 01");
                        Console.WriteLine();
                        Nomor01.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Nomor 02");
                        Console.WriteLine();
                        Nomor02.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("Nomor 04");
                        Console.WriteLine();
                        Nomor04.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("Nomor 05");
                        Console.WriteLine();
                        Nomor05.Resolve();
                        break;


                    case 6:
                        Console.WriteLine("Nomor 06");
                        Console.WriteLine();
                        Nomor06.Resolve();
                        break;

                    case 7:
                        Console.WriteLine("Nomor 07");
                        Console.WriteLine();
                        Nomor07.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("Nomor 08");
                        Console.WriteLine();
                        Nomor08.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("Nomor 10");
                        Console.WriteLine();
                        Nomor10.Resolve();
                        break;

                }

                Console.WriteLine("Continue?");
                answer = Console.ReadLine();
            }
        }
    }
}
