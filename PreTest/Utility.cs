﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Utility
    {
        public static int[] fibonacciArray(int length)
        {
            int[] fibonacciArray = new int[length];

            //memasukkan Value
            for (int i = 0; i < length; i++)
            {
                if (i <= 1)
                {
                    fibonacciArray[i] = 1;
                }
                else
                {
                    fibonacciArray[i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
                }
            }

            return fibonacciArray;
        }

        public static int[] primaArray(int length)
        {
            int[] primaArray = new int[length];

            int prima = 2;

            int lengthYangprima = 0;

            while (lengthYangprima < length)
            {
                int faktorPrima = 0;

                for (int j = 1; j <= prima; j++)
                {
                    if (prima % j == 0)
                    {
                        faktorPrima += 1;
                    }
                }

                if (faktorPrima == 2)
                {
                    primaArray[length] = prima;
                    lengthYangprima++;
                }

                prima++;
            }

            return primaArray;
        }

        public static void PrintArray(int[] array)
        {
            for (int i = 0; i < array.GetLength(0); i++)
            {
                Console.Write(array[i] + " ");
            }

            Console.WriteLine();
        }
    }
}
