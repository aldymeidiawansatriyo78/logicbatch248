﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Nomor04
    {
        public static void Resolve()
        {
            Console.Write("Saldo OPO anda adalah ");
            int opo = int.Parse(Console.ReadLine());

            int saldoAwalOpo = opo;
            int saldoAkhirOpo = 0;
            int hargaKopi = 18000;
            int diskonKopi = hargaKopi / 2;
            int cashbackOpo = hargaKopi / 10;
            int cashbackOpoDiskon = diskonKopi / 10;
            int jumlahCup = 0;

            if (opo < hargaKopi)
            {
                Console.WriteLine("Maaf saldo kurang");
            }
            else
            {
                if (saldoAwalOpo < 40000)
                {
                    while (saldoAwalOpo >= 18000)
                    {
                        saldoAwalOpo -= 18000;
                        saldoAkhirOpo += cashbackOpo;
                        jumlahCup++;
                    }
                }
                else
                {
                    while (saldoAwalOpo >= diskonKopi)
                    {
                        saldoAwalOpo -= diskonKopi;
                        saldoAkhirOpo += cashbackOpoDiskon;
                        jumlahCup++;
                    }

                    if ((diskonKopi * jumlahCup) > 100000)
                    {
                        Console.WriteLine("Maaf tidak bisa melebihi 100rb");
                    }
                }

                int totalAkhirSaldo = saldoAkhirOpo + saldoAwalOpo;
                Console.WriteLine("Jumlah cup yang dibeli adalah " + jumlahCup + " Sisa Saldo Akhir adalah " + totalAkhirSaldo);
            }          
        }
    }
}
