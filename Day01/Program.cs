﻿using System;

namespace Day01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input Nomor Soal");
            double nomorSoal = int.Parse(Console.ReadLine());

            if (nomorSoal == 1)
            {
                //Soal perhitungan BMI

                Console.WriteLine("Masukkan tinggi dalam satuan meter");
                int tinggi = int.Parse(Console.ReadLine());

                Console.WriteLine("Masukkan berat");
                double berat = int.Parse(Console.ReadLine());

                double BMI = berat / (tinggi * tinggi);

                if (BMI < 18.5)
                {
                    Console.WriteLine("Underweight");
                }
                else if (BMI > 25)
                {
                    Console.WriteLine("Overweight");
                }
                else
                {
                    Console.WriteLine("Normal");
                }
            }

            //Soal deret satu dimensi
            else if (nomorSoal == 2)
            {
                Console.WriteLine("masukkan panjang angka : ");
                int length = int.Parse(Console.ReadLine());

                ////Tugas 1

                int[] angkaGanjilArray = new int[length];
                int angkaGanjil = 1;

                ////memasukkan Value
                for (int i = 0; i < length; i++)
                {
                    angkaGanjilArray[i] = angkaGanjil;
                    angkaGanjil += 2;
                }

                ////cetak Value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(angkaGanjilArray[i] + "  ");
                }
                Console.WriteLine();

                //////Tugas 2

                int[] angkaGenapArray = new int[length];
                int angkaGenap = 2;

                ////memasukkan Value
                for (int i = 0; i < length; i++)
                {
                    angkaGenapArray[i] = angkaGenap;
                    angkaGenap += 2;
                }

                ////Cetak Value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(angkaGenapArray[i] + "  ");
                }
                Console.WriteLine();

                //////Tugas 3

                int lebihTiga = 1;
                for (int i = 0; i < length; i++)
                {
                    Console.Write(lebihTiga + "  ");
                    lebihTiga += 3;
                }
                Console.WriteLine();

                //////Tugas 4

                int[] lebihEmpatArray = new int[length];
                int lebihEmpat = 1;

                //memasukkan Value
                for (int i = 0; i < length; i++)
                {
                    lebihEmpatArray[i] = lebihEmpat;
                    lebihEmpat += 4;
                }

                //mencetak Value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(lebihEmpatArray[i] + "  ");
                }
                Console.WriteLine();

                //////Tugas 5 

                int bintangDua = 1;

                for (int i = 0; i < length; i++)
                {
                    Console.Write(bintangDua + "  ");
                    bintangDua += 4;

                    if (bintangDua % 3 == 0)
                    {
                        Console.Write("*  ");
                    }
                }
                Console.WriteLine();

                ////Tugas 6

                int bintangTiga = 1;

                for (int i = 0; i < length; i++)
                {
                    if ((i + 1) % 3 == 0)
                        Console.Write("*  ");
                    else
                        Console.Write(bintangTiga + "  ");
                    bintangTiga += 4;
                }
                Console.WriteLine();

                ////Tugas 7

                int perkalian2 = 2;

                for (int i = 0; i < length; i++)
                {
                    Console.Write(perkalian2 + "  ");
                    perkalian2 *= 2;
                }
                Console.WriteLine();

                ////Tugas 8

                int pangkat3 = 3;

                for (int i = 0; i < length; i++)
                {
                    Console.Write(pangkat3 + "  ");
                    pangkat3 *= 3;
                }
                Console.WriteLine();

                ////Tugas 9

                int perkalian4 = 1;

                for (int i = 0; i < length; i++)
                {
                    if ((i + 1) % 3 == 0)
                        Console.Write("*  ");
                    else
                        Console.Write(perkalian4 + "  ");
                    perkalian4 *= 4;
                }
                Console.WriteLine();

                ////Tugas 10

                int perkalianXXX = 3;

                for (int i = 0; i < length; i++)
                {
                    if ((i + 1) % 4 == 0)
                        Console.Write("XXX  ");
                    else
                        Console.Write(perkalianXXX + "  ");
                    perkalianXXX *= 3;
                }
                Console.WriteLine();

                //Tugas 11

                int[] fibonacciArray = new int[length];

                //memasukkan Value
                for (int i = 0; i < length; i++)
                {
                    if (i <= 1)
                    {
                        fibonacciArray[i] = 1;
                    }
                    else
                    {
                        fibonacciArray[i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
                    }
                }
                //cetakValue

                for (int i = 0; i < length; i++)
                {
                    Console.Write(fibonacciArray[i] + "  ");
                }
                Console.WriteLine();
                //Tugas 12

                int[] angkaBalikArray = new int[length];

                //memasukkan Value
                for (int i = 0; i < length; i++)
                {
                    if (length % 2 == 0)
                    {
                        if (i == 0)
                        {
                            angkaBalikArray[i] = 1;
                        }
                        else if (i < length / 2)
                        {
                            angkaBalikArray[i] = angkaBalikArray[i - 1] + 2;
                        }
                        else if (i == length / 2)
                        {
                            angkaBalikArray[i] = angkaBalikArray[i - 1];
                        }
                        else
                        {
                            angkaBalikArray[i] = angkaBalikArray[i - 1] - 2;
                        }
                    }

                    else

                    {
                        if (i < 1)
                        {
                            angkaBalikArray[i] = 1;
                        }
                        else if (i <= length / 2)
                        {
                            angkaBalikArray[i] = angkaBalikArray[i - 1] + 2;
                        }
                        else
                        {
                            angkaBalikArray[i] = angkaBalikArray[i - 1] - 2;
                        }
                    }
                }
                //cetak Value

                for (int i = 0; i < length; i++)
                {
                    Console.Write(angkaBalikArray[i] + "  ");
                }
                Console.WriteLine();

                //Tugas 13

                int[] loncatTigaArray = new int[length];

                //memasukkan Value

                for (int i = 0; i < length; i++)
                {
                    if (i < 3)
                    {
                        loncatTigaArray[i] = 1;
                    }
                    else 
                    {
                        loncatTigaArray[i] = loncatTigaArray[i - 1] + loncatTigaArray[i - 2] + loncatTigaArray[i - 3];
                    }
                }
                for (int i = 0; i < length; i++)
                {
                    Console.Write(loncatTigaArray[i] + "  ");
                }
                Console.WriteLine();

                //Tugas 14

                int bilanganPrima = 2;

                //memasukkan Value

                for (int i = 0; i < length; i++)
                {
                    if (bilanganPrima % bilanganPrima == 0)
                    {
                        Console.Write(bilanganPrima + "  ");
                        bilanganPrima += 1;
                    }
                    else
                    {
                        Console.Write("");
                    }                      
                }

                Console.WriteLine();

                //Tugas 15

                int[] fibonacciMirrorArray = new int[length];
                int fibonacciMirror = 1;

                //memasukkan Value

                for (int i = 0; i < length; i++)
                {                
                    if (length % 2 == 0)
                    {
                        if (i < length / 2 && i <= 1)
                        {
                            fibonacciMirrorArray[length - 1 - i] = fibonacciMirror;
                            fibonacciMirrorArray[i] = fibonacciMirror;
                                                    
                        }
                        else if(i < length / 2 && i > 1)
                        {
                            fibonacciMirrorArray[length - 1 - i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
                            fibonacciMirrorArray[i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
                        }    
                    }
                    else
                    {
                        if (i < length / 2 && i <= 1)
                        {
                            fibonacciMirrorArray[length - 1 - i] = fibonacciMirror;
                            fibonacciMirrorArray[i] = fibonacciMirror;                     
                         }
                        else if (i <= length / 2 && i > 1)
                        {
                            fibonacciMirrorArray[length - 1 - i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
                            fibonacciMirrorArray[i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
                        }
                    }
                }

                //Cetak Value

                for (int i = 0; i < length; i++)
                {
                    Console.Write(fibonacciMirrorArray[i] + "  ");
                }
            }
            else
            {
                Console.WriteLine("Lupa");
            }
            Console.ReadKey();
        }
    }
}

